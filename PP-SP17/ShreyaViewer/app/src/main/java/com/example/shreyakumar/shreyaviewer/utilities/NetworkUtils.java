package com.example.shreyakumar.shreyaviewer.utilities;

import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by shreyakumar on 2/20/17.
 */

public class NetworkUtils {
    private static final String REDDIT_URL = "https://www.reddit.com/r/";
    private static final String defaultTopic = "funny";
    private static final String newsTopic = "news";
    private static final String endFormat = "/.json";

    public static URL buildUrl(String userQuery){
        Uri builtUri = Uri.parse(REDDIT_URL).buildUpon()
                        .appendPath(userQuery)
                        .appendPath(endFormat)
                        .build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        }catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    } // end of function buildUrl

    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) return scanner.next();
            else return null;
        }finally {
            urlConnection.disconnect();
        }
    }   // end of function
}
