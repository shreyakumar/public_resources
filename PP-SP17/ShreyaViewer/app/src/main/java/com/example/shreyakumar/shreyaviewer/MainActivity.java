package com.example.shreyakumar.shreyaviewer;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.example.shreyakumar.shreyaviewer.utilities.NetworkUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private EditText mSearchBoxEditText;
    private TextView mDisplayTextView;
    private TextView mSearchResultsTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSearchBoxEditText      = (EditText) findViewById(R.id.et_search_box);
        mDisplayTextView        = (TextView) findViewById(R.id.tv_url_display);
        mSearchResultsTextView  = (TextView) findViewById(R.id.tv_search_results_json);
    } // end of onCreate

    private void makeSearchQuery(){
        String searchQuery = mSearchBoxEditText.getText().toString();
        mDisplayTextView.setText("Results for subreddit " + searchQuery + " :");
        mSearchResultsTextView.setText("");
        // actually perform search and get results
        new FetchNetworkData().execute(searchQuery);
    } // end of function make search

    //class to perform networking duties
    public class FetchNetworkData extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            if (params.length == 0) return null;
            //build query url
            String searchQuery = params[0];
            URL url = NetworkUtils.buildUrl(searchQuery);
            //get data from the query url
            String responseString = null;
            try {
                responseString = NetworkUtils.getResponseFromHttpUrl(url);
            }catch (Exception e){
                e.printStackTrace();
            }
            return responseString;

        } // end of function doInBackground

        @Override
        protected void onPostExecute(String responseData) {
            //super.onPostExecute(s);
            // process response data - which is a json formatted string
            String[] titles = processRedditJSON(responseData);
            // grab each title from reddit json and  append to search results
            for (String title : titles){
                mSearchResultsTextView.append(title + "\n\n\n");
            }
        }   //end of function

        public String[] processRedditJSON(String responseJSONData){
            String[] newsTitles = new String[25];
            try {
                // parse the JSON string to get titles
                JSONObject newsReddit = new JSONObject(responseJSONData);
                JSONObject newsObject = newsReddit.getJSONObject("data");
                JSONArray children = newsObject.getJSONArray("children");

                newsTitles = new String[children.length()];

                for (int i = 0; i < newsTitles.length; i++) {
                    JSONObject childJSON = children.getJSONObject(i);
                    JSONObject childData = childJSON.getJSONObject("data");
                    String title = childData.getString("title");
                    newsTitles[i] = title;
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return newsTitles;
        }   // end function processJSON

    }   // end of clas FetchNetworkData

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    } // end function

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemThatWasClicked = item.getItemId();
        if (itemThatWasClicked == R.id.action_search){
            makeSearchQuery();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }   // end function
}
