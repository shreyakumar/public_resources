
function RadioButton() {

    this.createRadioButton = function(name, labels, id){

        this.item = document.createElement("form");
        this.name = name;

        for ( var x in labels) {
            var lx = labels[x];
            var tmp = document.createElement("input");

            tmp.setAttribute("type", "radio");
            tmp.setAttribute("name", name);
            tmp.setAttribute("value", lx);
            //adding input element to form
            this.item.appendChild(tmp);
        }
    } // end of createRadioButton

    this.getValue = function() {
        var radios = this.item.elements[this.name];

        for (var i = 0; i < radios.length; i++){
            var r = radios[i];
            if (r.checked) return r.value;
        }
        return 'error';
    } // end of getValue function

    this.addToDocument = function() {
        document.body.appendChild(this.item);
    }   // end of function addToDocument
}
