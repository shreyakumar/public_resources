
document.getElementById("theButton").onmouseup = changeText;

var label = document.createElement("p");
label.setAttribute("id", "theLabel");

var labelText = document.createTextNode("who?");

var bird = { fly : function(){ alert("i'm flying!"); } };

var sparrow = { name : "Captain Jack", wingspan : 5};
var duck = {
	addClickHandler : function(handler, args){
		document.getElementById("theButton").onmouseup = function(){ handler(args); };
	},
	name : "Donald",
	wingspan : 22,
	quack : function() { alert("quack!"); }
};

//alert(duck.name);
//duck.quack();

args = ["first argument"];
function showTheMessage(args){
	alert(args[0]);
}

duck.addClickHandler(showTheMessage, args);


duck.__proto__ = bird;
//duck.fly();

label.appendChild(labelText);
document.body.appendChild(label);

function changeText()
{
	var tl = document.getElementById("theLabel");
	tl.innerHTML = "Shreya Kumar";
}
