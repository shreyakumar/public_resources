#Note: this document just describes incomplete code steps. you would have follow these steps an write your own code to get it to work. Consider what else you would need.

#step 1: initializing GameSpace
class GameSpace:
    def main(self):
        pygame.init()
        self.size = self.width, self.height = 640, 420
        self.black = 0,0,0
        self.screen = pygame.display.set_mode(self.size)

#step 2: initialize game objects
        self.player = Player(self)
        self.clock = pygame.time.Clock()

#step 3: start game loop
        while 1:
#step 4: tick regulation
            self.clock.tick(60)

#step 5: reading user input
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    self.player.move(event key)
#step 6: for every sprite/game object, call tick()
            self.player.tick()

#step 7: update the screen
            self.screen.fill(self.black)
            self.screen.blit(self.player.image, self.player.rect)
            self.screen.flip()

#later as part of step 1
if __name__ = `__main__`:
    gs = GameSpace()
    gs.main()
