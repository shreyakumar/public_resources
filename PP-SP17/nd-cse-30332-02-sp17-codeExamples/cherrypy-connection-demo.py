import cherrypy
import json

class DictionaryKeyController(object):

    def GET(self, key):
        output = {'result': 'success'} # could also be pending or failure, rule 1

        #rule 2: check your input data before you use it. key
        key = int(key) # or key = str(key)
        the_body = cherrypy.request.body.read().decode() #decode makes thebody a string.

        try:
            #something that can cause an exception - like key error in dictionary, some type matching error.
            the_body = json.loads(the_body)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

class DictionaryController(object):

    def GET(self):
        return json.dumps({"result": "quack"})

def start_service():

    myController = MyController() #just a place to keep a handler, classes that contains handlers are typically called controllers.

    dispatcher = cherrypy.dispatch.RoutesDispatcher() #dispatcher

    dispatcher.connect('duckconnection', 
            '/animals/:key', 
            controller=myController, 
            action='GET',
            conditions=dict(method=['GET'])) #connection_name, resource_name /duck/, controller which is the object that holds the handlers, the name of the function inside the controller class,    

    conf = {
            'global': {
                'server.socket_host':'ash.campus.nd.edu',
                'server.socket_port': 40151,
                },
            '/': {'request.dispatch': dispatcher}
            }
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app) #starts the webservice, event loop like

if __name__=='__main__':
    start_service()
