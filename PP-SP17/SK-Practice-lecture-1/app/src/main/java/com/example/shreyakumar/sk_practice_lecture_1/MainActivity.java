package com.example.shreyakumar.sk_practice_lecture_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Context;



public class MainActivity extends AppCompatActivity {

    private EditText mSearchBoxEditText;
    private TextView mDisplayTextView;
    private TextView mSearchResultsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSearchBoxEditText = (EditText) findViewById(R.id.et_search_box);
        mDisplayTextView = (TextView) findViewById(R.id.tv_display_text);
        mSearchResultsTextView = (TextView) findViewById(R.id.tv_results);



        /*
        mStudentListView = (TextView) findViewById(R.id.tv_name);
        mStudentListView.append("\n\n");

        String[] studentNames = {"Camilla", "Bryce", "Abby", "Taylor", "Allison", "Maria", "Alan", "Aidan", "Doug", "Andrew", "Charles", "Roann", "Rita", "Kevin", "Maru", "Chris", "Ivy", "John"};
        for (String studentName : studentNames){
            mStudentListView.append(studentName + "\n\n\n\n");
        }
        */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    } // end of onCreateOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemSelected = item.getItemId();

        //if our search option was selected
        if(menuItemSelected == R.id.action_search){
            Context context = MainActivity.this;
            String message = "Search selected";
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
        return true;
    }



}
