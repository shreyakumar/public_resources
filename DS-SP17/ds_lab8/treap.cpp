// treap.cpp: Treap Map

#include "map.h"

#include <climits>
#include <random>

// Prototypes - helper functions ------------------------------------------------------------------

extern void dump_r(Node *node, std::ostream &os, DumpFlag flag);

// Methods ---------------------------------------------------------------------

void            TreapMap::insert(const std::string &key, const std::string &value) {
    //TODO
}

const Entry     TreapMap::search(const std::string &key) {
    //TODO
}

void            TreapMap::dump(std::ostream &os, DumpFlag flag) {
    dump_r(root, os, flag);
}

int             TreapMap::getMaxTreeHeight(){
    //TODO
}


int             TreapMap::getMinTreeHeight(){
    //TODO
}

void            TreapMap::preOrderTraversal(){
    //TODO
}


// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
