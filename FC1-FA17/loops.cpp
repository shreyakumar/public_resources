#include <iostream>

using namespace std;

int main(){
    int n = 0;

    //iterating with while
    while (n < 10){
        cout << n << endl;
        n = n + 1;
    }
    cout << "Now n is: " << n << endl;

    cout << "\n\n Using while to calculate total" << endl;
    n = 0;
    int total = 0;

    while (n < 6){
        total = total + n;
        n = n + 1; // if you miss this line, infinite loop.
        cout << "N : " << n << " Total: " << total << endl;
    }
    cout << "Sum of the first " << (n - 1) << " numbers is: " << total << endl;

    cout << "\n\n Using for" << endl;
    //iterating with for
    for (total = 0, n = 0; n < 10; n++){
        total = total + n;
        cout << "N: " << n << " Total: " << total << endl;
    }

    //nested for loops
    for (int i = 0; i < 5; i++){
        cout << "i = " << i << endl; // when something should happen just when i flips.
        for (int j = 0; j < 3; j++){

            if (j == 0) cout << "**" << endl;
            else if (j == 1) cout << "****" << endl;
            else if (j == 2) cout << "******" << endl;
            //cout << "i: " << i << " j: " << j << endl;
        }
    }

    //nested for loops - reverse
    cout << "reverse: " << endl;
    for (int i = 1; i <= 5; i++) {
        for (int j = 1; j <= (5 - i); j++) {
            cout << " ";
        }
        for (int k = 1; k <= i; k++) {
            cout << i;
        }
        cout << endl;
    }

    //nested for loops - mulitplication table for lab2
    cout << "Enter x y: " ;
    int x = 0, y = 0;
    cin >> x >> y;

    for (int i = 0; i<x; i++){
        for (int j = 0; j<y; j++){
            cout << "   " << i << ", " << j;
        }
        cout << endl;
    }

    //while with counter
    int tries = 10;
    while (tries > 0){
        cout << "tries is : " << tries << endl;
        tries--;
    }

    //while with user input
    char answer = 'y';
    int counter = 1;
    while(answer == 'y'){
        cout << "On trial " << counter << endl;
        counter++;
        cout << "Continue? (y/n): ";
        cin >> answer;
        if (answer == 'n') break;
    }

    //gcd
    int n1 = 0, n2 = 0;
    cout << "Enter 2 integers:";
    cin >> n1 >> n2;
    int gcd = 1; //init
    int k = 2; // possible starting gcd
    while(k <= n1 && k <= n2){
        if(n1 % k == 0 && n2 % k == 0){
            gcd = k; // found gcd
        }
        k++; //try next k
    }
    cout << "The gcd for " << n1 << " and " << n2 << " is " << gcd << endl;

    //predicting future tuition
    double tuition = 10000;
    int year = 1;
    cout << "Year   Tuition" << endl;
    while( tuition < 20000){ // stop when it doubles
        tuition = tuition * 1.07;
        cout << year << "    " << tuition << endl;
        year++;
    }
    cout << "Tuition doubles after " << year-1 << " years" << endl;

    return 0;
}
