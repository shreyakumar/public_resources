#include <iostream>

using namespace std;

int main(){

    int length = 0, width = 0;
    bool result = false;

    // taking input
    cout << "Enter a length: " ;
    cin >> length;
    cout << "Enter a width: ";
    cin >> width;

    // printing input lenth and width
    cout << "The length you entered is :" << length << " and the width is: " << width << endl;

    //compare length and width
    cout << "length < width : " << (length < width) << endl;
    cout << "length > width : " << (length > width) << endl;
    cout << "length == width : " << (length == width) << endl;
    cout << "length != width : " << (length != width) << endl;
    cout << "length == width*width : " << (length == width*width) << endl;

    result = (length == width*2);
    cout << "length == width*2 : " << result << endl;
    cout << "not (length == width*2) : " << !result << endl;

    //using && and ||
    cout << "0 < length && length < 10 :" << (0 < length && length < 10) << endl;
    cout << "0 > length || length > 10 :" << (0 > length || length > 10) << endl;

    //using if
    if (0 < length) {
        cout << "Length is greater than 0";
        if (length < 10){
            cout << " and length is less than 10" << endl;
        }
    }
    else if (length < 10) {
        cout << "length is less than 10" << endl; // why wont this print?? because even though this condition is true, it only executes one from the if conditional cases.

    }
    else {
        cout << "We are in else!";
    }

    //length positive/negative/zero
    if (length%2 == 0) {
        cout << "Length is even!" << endl;
    }
    else {
        cout << "Lenght is odd!" << endl;
    }

    //checking positive/negative/zero
    if (length < 0) {
        cout << "Length is negative!" << endl;
    }
    else if (length == 0) {
        cout << "Lenght is zero!" << endl;
    }
    else {
        cout << "Length is positive" << endl;
    }

    //positive/negative nested conditional
    if (length < 0) {
        cout << "Length is negative!" << endl;
    }
    else { // first and outer else
        if (length == 0) {
            cout << "Length is zero!" << endl;
        }
        else { // inner else
            cout << "Length is positive" << endl;
        } //end of inner else
    } // end of first else

    //odd and divisible by 3
    if (width % 2 == 1) {//odd
        if (width % 3 == 0) { //divisible by 3
            cout << "width is odd and divisible by 3" << endl;
        }
    }

    //Write a program that requests the grade (out of 100) and the prints out the corresponding letter grade. > 90 A , > 80 B, > 70 C, > 65 D, F

    return 0;
}
